// var concatCss = require("gulp-concat-css");
var concat = require("gulp-concat");
// var clean = require("gulp-clean");
var rename = require("gulp-rename");
var watch = require("gulp-watch");
var postcss = require("gulp-postcss");
// var cssnext = require("postcss-cssnext");
var order = require("gulp-order");
// var hwb = require("postcss-color-hwb");
var properties = require("postcss-custom-properties");
var color = require("postcss-color-function");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var csso = require("gulp-csso");
var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var async = require("async");
var iconfont = require("gulp-iconfont");
var consolidate = require("gulp-consolidate");
// var svgstore = require("gulp-svgstore");
var svgmin = require("gulp-svgmin");
// var fs = require("fs");
var svgSymbols = require("gulp-svg-symbols");

var runTimestamp = Math.round(Date.now() / 1000);

const AUTOPREFIXER_BROWSERS = [
  "ie >= 11",
  "ff >= 30",
  "chrome >= 34",
  "safari >= 7"
];

gulp.task("sprites", function() {
  return gulp
    .src("icons/src/**/*.svg")
    .pipe(svgSymbols())
    .pipe(gulp.dest("icons"));
});

gulp.task("svg", function() {
  return gulp
    .src("icons/src/**/*.svg")
    .pipe(
      svgmin({
        plugins: [
          {
            removeTitle: true
          },
          {
            removeDoctype: true
          },
          {
            removeUselessStrokeAndFill: true
          },
          {},
          {
            removeAttrs: { attrs: "(stroke|fill)" }
          },
          {
            collapseGroups: true
          }
        ],
        js2svg: {
          pretty: true
        }
      })
    )
    .pipe(
      svgSymbols({
        templates: [`default-svg`]
      })
    )
    .pipe(rename("astro-icons.min.svg"))
    .pipe(gulp.dest("icons"));
});

gulp.task("icons", ["svg"], function(done) {
  var iconStream = gulp.src(["icons/src/**/*.svg"]).pipe(
    iconfont({
      fontName: `astro-icons`,
      formats: ["ttf", "woff", "woff2"]
    })
  );

  async.parallel(
    [
      function handleGlyphs(cb) {
        iconStream.on("glyphs", function(glyphs, options) {
          gulp
            .src("templates/myfont.css")
            .pipe(
              consolidate("lodash", {
                glyphs: glyphs,
                fontName: "Astro",
                fontPath: "../icons/src",
                fontHeight: 1001,
                normalize: true
              })
            )
            .pipe(gulp.dest("icons/"))
            .on("finish", cb);
        });
      },
      function handleFonts(cb) {
        iconStream.pipe(gulp.dest("icons/")).on("finish", cb);
      }
    ],
    done
  );
});

// gulp.task("icons", gulp.series("svg", "icon-fonts"));

gulp.task("color", function() {
  return gulp
    .src("css/colors/**/*.css")
    .pipe(
      postcss([
        properties({ warnings: true, strict: false, preserve: "computed" }),
        color()
      ])
    )
    .pipe(gulp.dest("./css/src"));
});

gulp.task("demosUpdated", function() {
  return gulp.src("demos/*.css").pipe(browserSync.stream());
});

gulp.task("packagesUpdated", function() {
  return gulp.src("packages/**/*.js").pipe(browserSync.reload());
});

gulp.task("cssUpdated", function() {
  return gulp.src("css/astro.css").pipe(browserSync.stream());
});

gulp.task("watch", function() {
  gulp.watch("css/colors/**/*.css", ["color"]);
  gulp.watch("css/src/**/*.css", ["concatCss", "concatCoreCss"]);
  gulp.watch("packages/**/*.js", ["packagesUpdated"]);
  gulp.watch("css/astro.css", ["cssUpdated"]);
  gulp.watch("demos/*.css", ["demosUpdated"]);
  return watch("./css/src/**/*.css", {
    ignoreInitial: false
  });
});

// gulp.task('removeCss', function() {
//   return gulp.src('css/astro.css', {read: false})
//   .pipe(clean())
// })

gulp.task("concatCss", function() {
  return gulp
    .src("css/src/**/*.css")
    .pipe(
      order([
        "css/src/_colors",
        "css/src/_fonts.css",
        "css/src/_variables.css",
        "css/src/_theme--light.css",
        "css/src/_theme--dark.css",
        "css/src/_typography.css",
        "css/src/_global.css",
        "css/src/**/*.css"
      ])
    )
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    .pipe(postcss([properties()]))
    .pipe(concat("astro.css"))
    .pipe(gulp.dest("css"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(csso())
    .pipe(gulp.dest("css"))
    .pipe(sourcemaps.write("./"));
});

gulp.task("concatCoreCss", function() {
  return gulp
    .src(["css/src/**/*.css", "!css/src/components/**/*"])
    .pipe(
      order([
        "css/src/_colors",
        "css/src/_fonts.css",
        "css/src/_variables.css",
        "css/src/_theme--light.css",
        "css/src/_theme--dark.css",
        "css/src/_typography.css",
        "css/src/_global.css",
        "css/src/**/*.css"
      ])
    )
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
    .pipe(postcss([properties()]))
    .pipe(concat("astro.core.css"))
    .pipe(gulp.dest("css"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(csso())
    .pipe(gulp.dest("css"))
    .pipe(sourcemaps.write("./"));
});

gulp.task("browser-sync", function() {
  browserSync.init({
    options: {
      "no-open": true
    },
    server: {
      baseDir: "./"
    }
  });
});

gulp.task("default", ["browser-sync", "watch", "color"]);
// gulp.task("default", ["browser-sync"]);
