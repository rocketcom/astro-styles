#Checkbox
A standard Checkbox in the Astro Design Language
##Guidelines

* [Astro UXDS: Checkbox](http://www.astrouxds.com/library/checkbox)

##Usage
Checkboxes input elements and their associated labels should have a containing element with a class of `rux-checkbox`, the element should be a block level element. A semantically valid practice in forms with specific steps is to place your form elements inside of an ordered list (`<ol>`) and is shown in the sample below.

```html
<li class='rux-checkbox'>
  <input type='checkbox' name='checkbox1c' id='checkbox1c'>
  <label for='checkbox1c'>Checkbox</label>
</li>
```

###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `disabled`     | Sets the checkbox to disabled. Interactions are prevented, visual style and icon both indicate disabled status. Works with all variants |