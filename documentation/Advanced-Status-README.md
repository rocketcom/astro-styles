#Advanced Status Symbol
Simple Status Symbols from the Astro UXDS Guidelines.

##Guidelines
For guidance on color, behavior and expected usage refer to the Astro UXDS Guidelines

* [Astro UXDS: Status Symbols](http://www.astrouxds.com/library/icon-symbols)

##Usage
Advanced Status Symbols are more complex, data rich versions of the standard [Status Symbols](http://www.astrouxds.com/library/status-symbols).

```html
<div class='rux-advanced-status rux-status--off'>
  <div class="rux-advanced-status__icon-group">
    <div class="rux-advanced-status__icon">
      <svg class="rux-icon" viewBox="0 0 114 114" preserveAspectRatio="xMidYMid meet">
        <use xlink:href="#mission" />
      </svg>
    </div>
    <div class="rux-advanced-status__badge">4</div>
  </div>
  <div class="rux-advanced-status__label">Misson
    <span class="rux-advanced-status__label__sub-label">Sub Label</span>
  </div>
</div>
```

__Note__: Label, sub-label and badge are all optional. Advanced Status Symbols are provided in Astro Styles as reference, to get the full benefit they require JavaScript to provide timely updates to status and notifications.

###Classes/Variants
| Class | Description  
| --------------- | ------- |
| `rux-status--[status]` | [status] is a placeholder, valid options include `off`,`standby`,`ok`,`caution`,`error` and `alert` |
