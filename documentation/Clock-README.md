# Clock

Presentation markup and styles for displaying a clock typically used in the Global Status Bar of an Astro Compliant Application. Note: the included HTML/CSS are presentation layer only, functionality must be provided by the developer.

## Guidelines

- [Astro UXDS: Clock](http://www.astrouxds.com/library/clock)

## Usage

Clock is provided for reference in implementing style and structure, JavaScript is required for full usage. For a functional Clock implementation please see the [Astro Web Component example](https://bitbucket.org/rocketcom/astro-components/src/master/packages/rux-clock/)

```html
<div class="rux-clock">
  <div class="rux-clock__segment rux-clock__day-of-the-year">
    <div class="rux-clock__segment__value" aria-labeledby="rux-clock__day-of-year-label">112</div>
    <div class="rux-clock__segment__label" id="rux-clock__day-of-year-label">Date</div>
  </div>
  <div class="rux-clock__segment rux-clock__time">
    <div class="rux-clock__segment__value" aria-labeledby="rux-clock__time-label">20:21:23 UTC</div>
    <div class="rux-clock__segment__label" id="rux-clock__time-label">Time</div>
  </div>
  <div class="rux-clock__segment rux-clock__aos">
    <div class="rux-clock__segment__value" aria-labeledby="rux-clock__time-label">18:11:00</div>
    <div class="rux-clock__segment__label" id="rux-clock__time-label">AOS</div>
  </div>
  <div class="rux-clock__segment rux-clock__los">
    <div class="rux-clock__segment__value" aria-labeledby="rux-clock__time-label">22:00:00</div>
    <div class="rux-clock__segment__label" id="rux-clock__time-label">LOS</div>
  </div>
</div>
```
