# Drop Down

Presentation markup and styles for displaying a Drop Down menu. Note: the included HTML/CSS are presentation layer only, functionality must be provided by the developer. Drop Down menus are heavily styled select elements.

## Guidelines

- [Astro UXDS: Drop Down](http://www.astrouxds.com/library/drop-down)

```html
<select class="rux-select">
  <optgroup label="Group One">
    <option>Group One Option One</option>
    <option>Group One Option Two</option>
    <option>Group One Option Three</option>
    <option>Group One Option Four</option>
  </optgroup>
  <optgroup label="Group Two">
    <option>Group Two Option One</option>
    <option>Group Two Option Two</option>
    <option>Group Two Option Three</option>
    <option>Group Two Option Four</option>
  </optgroup>
</select>
```
