#Slider
A standard Slider element in the Astro Design Language

##Guidelines
* [Astro UXDS: Slider](http://www.astrouxds.com/library/slider)

##Usage
The slider uses the [HTML5 Range Input Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/range) and inherits all the properties of the native element.

__Note__: Implementation require browesr specific styling and may vary from browser to browser.

```html
<input type='range' min='0' max='100' value='50' class='rux-range'>
```

###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `disabled`     | Sets the range disabled. Interactions are prevented, visual style indicates disabled status. |
| `min`     | Minimum value of the slider |
| `max`     | Maximum value of the slider |
| `step`     | Increment values. __Note__: If you want a floating point value set step to 0.1 |
| `value`     | Current value |