#Button
A standard Button in the Astro Design Language
##Guidelines

* [Astro UXDS: Button](http://www.astrouxds.com/library/buttons)

##Usage

```html
<button class='rux-button'>Button</button>
<button class='rux-button rux-button--default'>Default</button>
<button class='rux-button rux-button--small'>Small</button>
<button class='rux-button rux-button--large'>Large</button>
<button class='rux-button' disabled>Disabled</button>
```

###Classe/Variants
| Class        | Description                                                                                                                                                                      
| --------------- | ------- |
| `rux-button--default`     | Adds a 1 pixel outline indicating the button is the default behavior. Ensure any scripting/focus neccessary is applied to match expected behavior|
| `rux-button--small`     | A small button variant |
| `rux-button--large`     | A large button variant |


###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `disabled`     | Sets the button disabled. Interactions are prevented, visual style and icon both indicate disabled status. Works with all variants |