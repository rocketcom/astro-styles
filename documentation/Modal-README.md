#Dialog/Modal Window
A dialog box/modal window sample
##Guidelines

- [Astro UXDS: Button](http://www.astrouxds.com/library/dialog-box)

##Usage
This HTML/CSS sample is a display only implementation.

```html
<dialog class="rux-modal" aria-role="modal">

	<header class="rux-modal__titlebar">
		<h1>Modal Title</h1>
	</header>

	<div class="rux-modal__content">
		<div class="rux-modal__message">Release Modem 2 on slice 1000 for deactivation. Releasing this modem cannot
			be
			undone.
		</div>
		<div class="rux-button-group">
			<button class="rux-button rux-button--outline" data-value="false">Cancel</button>
			<button class="rux-button" data-value="true">Release</button>
		</div>
	</div>

</dialog>
```
