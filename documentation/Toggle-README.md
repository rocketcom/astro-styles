#Toggle Button
Toggle and push button style input elements from the Astro UXDS Guidelines
##Guidelines

* [Astro UXDS: Toggle](http://www.astrouxds.com/library/toggle)

##Usage
Both the toggle and push button style elements are based on standard HTML checkbox input types. Both require a containing `div` for the `input` and `label` elements and have the same functionality and requirements as a standard checkbox. 

###Toggle Button
The toggle button requires a container `div` with a class of `rux-toggle`. Toggle buttons have a pre-defined label set of __on__ and __off__ and cannot be changed. It is recommended an additional label or element of some sort be used in conjunction with `rux-toggle` as a label for the toggle element. The [aria-labeledby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-labelledby_attribute) attribute is a good place to start.

```html
 <div class='rux-toggle'>
	<input class='rux-toggle__input'id='toggle1' type='checkbox'>
	<label class='rux-toggle__button' for='toggle1'></label>
</div>
```

###Push Button
The push button requires a container `div` with a class of `rux-pushbutton`. Push buttons have definable labels for both the __on__ and __off__ state. These are set inside the `label` element with `<span class='off'>` and `<span class='on'>`. It is recommended an additional label or element of some sort be used in conjunction with `rux-toggle` as a label for the toggle element. The [aria-labeledby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-labelledby_attribute) attribute is a good place to start.


```html
 <div class='rux-pushbutton'>
	<input class='rux-pushbutton__input'id='toggle1' type='checkbox'>
	<label class='rux-pushbutton__button' for='toggle1'><span class='off'>Disabled</span><span class='on'>Enabled</span></label>
</div>
```
