#Tables
A standard Table element in the Astro Design Language

__Note__: Tables in their current form are deprecated. Use at your discretion.
##Guidelines

* [Astro UXDS: Tables](http://www.astrouxds.com/library/tables)

##Usage

```html
<table class='rux-table'>
  <thead>
    <tr class='rux-table__column-head'>
      <th>Modem</th>
      <th>Status</th>
      <th>Data</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>A</td>
      <td>B</td>
      <td>C</td>
    </tr>
    <tr class='rux-selected'>
      <td>A</td>
      <td>B</td>
      <td>C</td>
    </tr>
    <tr>
      <td>A</td>
      <td>B</td>
      <td>C</td>
    </tr>
  </tbody>
</table>
```