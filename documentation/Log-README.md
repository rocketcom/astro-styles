# Log

Presentation markup and styles for displaying a Log typically used in the Global Status Bar of an Astro Compliant Application. Note: the included HTML/CSS are presentation layer only, functionality must be provided by the developer.

## Guidelines

- [Astro UXDS: Log](http://www.astrouxds.com/library/log)

## Usage

Log is provided for reference in implementing style and structure, JavaScript is required for full usage. For a functional Log implementation please see the [Astro Web Component example](https://bitbucket.org/rocketcom/astro-components/src/master/packages/rux-log/)

```html
<div class="rux-log">
  <header class="rux-log-header">
    <h1 class="rux-log-header-title">Event Logs</h1>
    <ul class="rux-log__header-labels rux-row">
      <li class="log-event__timestamp">Time</li>
      <li class="log-event__status"></li>
      <li class="log-event__message log-header__message">
        Event
        <div class="rux-form-field rux-form-field--small">
          <input class="rux-form-field__text-input" placeholder="Search …" type="search" />
        </div>
      </li>
    </ul>
  </header>

  <ol class="rux-log__events">
    <!-- 
	A single log entry is comprised of a timestamp, status and message.
	Repeat for each log item
    //-->
    <li class="rux-log__log-event">
      <div class="log-event__timestamp">
        <time datetime="2018-7-10 19:19:59:424">19:19:59</time>
      </div>
      <div class="log-event__status">
        <div class="rux-status rux-status--off"></div>
      </div>
      <div class="log-event__message">
        <div>Reiciendis similique earum qui quas corporis error et</div>
      </div>
    </li>
    …
  </ol>
</div>
```
