#Icons
Default Icon Library for Astro

##Guidelines

* [Astro UXDS: Icons and Symbols](http://www.astrouxds.com/library/icons-and-symbols)

##Usage
Icons are provided as a single SVG library, individual SVG icons and as an icon font. Recommended usage is the combined SVG library. Icon Font usage is discouraged.

To ensure the widest browser support copy and paste the contents of `/icons/astro-icons.min.svg` in to the document where you plan to make use of an icon. Linking to the file may cause unexpected results in some browsers.


To use an icon use an SVG element with a class of `rux-icon` and a reference to the icon's id attribute.

```html
<svg class="rux-icon" viewBox="0 0 128 128" preserveAspectRatio="xMidYMid meet">
	<use xlink:href="#settings"></use>
</svg>
```

SVG Icons can be manipulated via CSS including the ability to change the icon color via the `fill` property.


##Additional Resources
Source art for icons are available as individual icon SVGs in `/icons/src/`, [Bohemain Codind Sketch](https://bitbucket.org/rocketcom/astro-styles/src/master/icons/src/astro-icons.sketch.zip) format and an [Adobe XD format]((https://bitbucket.org/rocketcom/astro-styles/src/master/icons/src/astro-icons.xd.zip))

##Generating a New Library
You can add to the standard Astro Icon Library via an included build tool with Astro 3.0. To make use of the build tool you will need both **node** and **gulp** installed.

Ensure you have gulp installed both local to your curerent working directory and globally, navigate to the directory containing the Astro Style Library and install dependencies via

```terminal
npm -i
```

Add any new icons to the `/icons/src/` directory ensuring the format matches the existing format of icons. Note: using the Sketch or XD library as a starting place will make it more likely your icons adhere to the necessary size and format. Give your SVG a unique file name as this is the **id** you will use to reference it in Astro e.g., **enterprise.svg** would create an icon entry with an id of **enterprise**

In your project directory open a terminal window and run

```terminal
gulp icons
```
This will optimize and concatenate a new icons in to a single SVG library you can use in your project. It will also generate a new icon font file. Note: Icon font rely on unicode character sets in CSS, generating a new icon font will require editing the icon font css

###List of Included Icons
| Icon | Sample | Id |
|------|--------| --- |
| Antenna Receive | ![Antenna Receive Icon](/documentation/antenna-receive.png) | #antenna-receive
| Antenna Off | ![Antenna Off Icon](/documentation/img/antenna-off.png) | #antenna-off
| Antenna Transmit | ![Antenna Transmit Icon](/documentation/img/antenna-transmit.png) | #antenna-transmit
| Antenna | ![Antenna Icon](/documentation/img/antenna.png) | #antenna
| Altitude | ![Altitude Icon](/documentation/img/altitude.png) | #altitude
| Caution | ![Caution Icon](/documentation/img/caution.png) | #caution
| Mission | ![Mission Icon](/documentation/img/mission.png) | #mission
| Netcom | ![Netcom Icon](/documentation/img/netcom.png) | #netcom
| Notifications | ![Notifications Icon](/documentation/img/notifications.png) | #notifications
| Payload | ![Payload Icon](/documentation/img/payload.png) | #payload
| Processor | ![Processor Icon](/documentation/img/processor.png) | #processor
| Processor Alt | ![Processor Alt Icon](/documentation/img/processor-alt.png) | #processor-alt
| Propulsion Power | ![Propulsion Power Icon](/documentation/img/propulsion-power.png) | #propulsion-power
| Satellite Receive | ![Satellite Receive Icon](/documentation/img/satellite-receive.png) | #satellite-receive
| Satellite Off | ![Satellite Off Icon](/documentation/img/satellite-off.png) | #satellite-off
| Satellite Transmit | ![Satellite Transmit Icon](/documentation/img/satellite-transmit.png) | #satellite-transmit
| Settings | ![Settings Icon](/documentation/img/settings.png) | #settings
| Thermal | ![Thermal Icon](/documentation/img/thermal.png) | #thermal
| Maintenance | ![Maintenance Icon](/documentation/img/maintenance.png) | #maintenance
| Collapse | ![Collapse Icon](/documentation/img/collapse.png) | #collapse
| Expand | ![Expand Icon](/documentation/img/expand.png) | #expand
| Unlock | ![Unlock Icon](/documentation/img/unlock.png) | #unlock
| Lock | ![Lock Icon](/documentation/img/lock.png) | #lock
| Add Small | ![Add Small Icon](/documentation/img/add-small.png) | #add-small
| Add Large | ![Add Large Icon](/documentation/img/add-large.png) | #add-large
| Remove Small | ![Remove Small Icon](/documentation/img/remove-small.png) | #remove-small
| Remove Large | ![Remove Large Icon](/documentation/img/remove-large.png) | #remove-large
| Close | ![Close Icon](/documentation/img/close-large.png) | #close-large
| Close Small | ![Close Small Icon](/documentation/img/close-small.png) | #close-small
| Search | ![Search Icon](/documentation/img/search.png) | #search