#Input Field
A standard input field in the Astro Design Language
##Guidelines

* [Astro UXDS: Input Field](http://www.astrouxds.com/library/input-field)

##Usage
Input elements and their associated labels should have a containing element with a class of `rux-form-field`. If the input is required insert and additional `<abbr class="required">*</abbr>` inside the `<label>` element to displan an asterisk. __Note__: The Astro CSS Library doesn’t handle validation rules or functionality only visual language.

```html
<!-- Standard Label/Input Form Field //-->
<div class="rux-form-field">
  <label>Standard input field</label>
  <input type="text" class="rux-text-entry">
  <span class="validity"></span>
</div>

<!-- Required Label/Input Form Field //-->
<div class="rux-form-field">
  <label><abbr class="required">*</abbr>Required input field</label>
  <input type="text" class="rux-text-entry">
  <span class="validity"></span>
</div>
```

###Tip
Sample show standard input, but styles apply to all valid HTML5 input field types. Take advantage of built in data type checking by using the appropriate input `type` and params. E.g., if expecting an email address use `<input type="email">` or a number with a minimum and maximum value use `<input type="number" min="10" max="100">`. See [Input Elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input) for a full description of available options.

###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `disabled`     | Sets the button disabled. Interactions are prevented, visual style and icon both indicate disabled status. Works with all variants |