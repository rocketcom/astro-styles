#Pop Up Menu
A Pop Up Menu in the Astro Design Language
##Guidelines

* [Astro UXDS: Pop Ups](http://www.astrouxds.com/library/pop-ups)

##Usage
Pop Ups are provided for style and structure only. Implementation requires JavaScript.

```html
<div class='rux-pop-up rux-pop-up--left'>
	<ul>
	  <li><a href>Option One</a></li>
	  <li><a href>Option Two</a></li>
	  <li><a href>Option Three</a></li>
	  <li><a href>Option Four</a></li>
	</ul>
</div>
```

###Classes/Variants
__Note__: Pop Up menus without a variant will have no indication caret. This is the intended behavior.

| Class        | Description                                                                                                                                                                      
| --------------- | ------- |
| `rux-pop-up--top`     | A pop up menu with the indicator caret at the top |
| `rux-pop-up--bottom`     | A pop up menu with the indicator caret at the bottom |
| `rux-pop-up--left`     | A pop up menu with the indicator caret at the left |
| `rux-pop-up--right`     | A pop up menu with the indicator caret at the right |
