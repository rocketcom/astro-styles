# Notifications

Presentation markup and styles for notification banners.

## Guidelines

- [Astro UXDS: Notifications](https://astrouxds.com/ui-components/notification-banner)

## Usage

Notification Banner is provided for reference in implementing style and structure only, JavaScript is required for functional usage. For a working Notification implementation please see the [Astro Web Component example](https://bitbucket.org/rocketcom/astro-components/src/master/packages/rux-notification/)

```html
<div class="rux-notification rux-notification--critical">
  <div class="rux-notification__message">This is critical alert notification banner.</div>
  <button class="rux-notification_close-button" title="Close"></button>
</div>
```

### Options

| Class                        | Description                                                                              |
| ---------------------------- | ---------------------------------------------------------------------------------------- |
| `rux-notification--critical` | Displays a notification banner intended to convey a critical error or failure            |
| `rux-notification--caution`  | Displays a notification banner intended to convey a caution notification                 |
| `rux-notification--normal`   | Displays a notification banner intended to convey a positive or affirmative notification |
| `rux-notification--info`     | Displays a notification with no implicit or implied status                               |

See [Astro UXDS: Status System](https://astrouxds.com/design-guidelines/status-system) for guidance on when to use status in your notification element.
