# Segmented Button

Presentation markup and styles for displaying a Segmented Button. Note: the included HTML/CSS are presentation layer only, functionality must be provided by the developer. Segmented Buttons are heavily styled radio button groups.

## Guidelines

- [Astro UXDS: Segmented Button](http://www.astrouxds.com/library/segmented-button)

## Usage

Segmented Button is provided for reference in implementing style and structure, JavaScript is required for full usage. For a functional Log implementation please see the [Astro Web Component example](https://bitbucket.org/rocketcom/astro-components/src/master/packages/rux-segmented-button/)

```html
<ul class="rux-segmented-button">
  <li class="rux-segmented-button__segment">
    <input type="radio" name="rux-group" id="segment1" />
    <label for="segment1">Segment 1</label>
  </li>
  <li class="rux-segmented-button__segment">
    <input type="radio" name="rux-group" id="segment2" />
    <label for="segment2">Segment 2</label>
  </li>
  <li class="rux-segmented-button__segment">
    <input type="radio" name="rux-group" id="segment3" />
    <label for="segment3">Segment 3</label>
  </li>
</ul>
```
