#Status Symbol
Simple Status Symbols from the Astro UXDS Guidelines.

##Guidelines
For guidance on color, behavior and expected usage refer to the Astro UXDS Guidelines

* [Astro UXDS: Status Symbols](http://www.astrouxds.com/library/status-symbols)

##Usage
Status Indicators are simple empty `divs`. A full compliment of all possible statuses are below.

```html
<div class='rux-status rux-status--off'></div>
<div class='rux-status rux-status--standby'></div>
<div class='rux-status rux-status--ok'></div>
<div class='rux-status rux-status--caution'></div>
<div class='rux-status rux-status--error'></div>
<div class='rux-status rux-status--alert'></div>
```

###Classes/Variants
| Class | Description  
| --------------- | ------- |
| `rux-status--[status]` | [status] is a placeholder, valid options include `off`,`standby`,`ok`,`caution`,`error` and `alert` |
