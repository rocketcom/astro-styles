#Tabs
Tab implementation based on the Astro Design Language
##Guidelines

* [Astro UXDS: Tabs](http://www.astrouxds.com/library/tabs)

##Usage
Tabs are provided for reference in implementing style and structure, JavaScript is required for full usage. For a functional Tabs implementation please see the [Astro Web Component example](https://bitbucket.org/rocketcom/astro-components/src/master/src/astro-components/rux-tabs/)

```html
<nav class='rux-tabs'>
	<ul>
	  	<li><a href='#'>Step 1</a></li>
	  	<li class='selected'><a href>Execute</a></li>
	  	<li><a href='#'>Monitor</a></li>
	  	<li><a href='#'>Analyze</a></li>
	</ul>
</nav>
```

###Classes/Variants
| Class        | Description                                                                                                                                                                      
| --------------- | ------- |
| `selected`     | Indicates the selected tab |
