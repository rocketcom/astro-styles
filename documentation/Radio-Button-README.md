#Radio Button
A standard Radio Button in the Astro Design Language
##Guidelines

* [Astro UXDS: Radio Button](http://www.astrouxds.com/library/radio-button)

##Usage
Radio Button input elements and their associated labels should have a containing element with a class of `rux-radio-button`, the element should be a block level element. A semantically valid practice in forms with specific steps is to place your form elements inside of an ordered list (`<ol>`) and is shown in the sample below.

```html
<li class='rux-radio-button'>
  <input type='radio' name='checkbox-sample' id='checkbox1'>
  <label for='checkbox1'>Checkbox</label>
</li>
```

###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `disabled`     | Sets the radio button to disabled. Interactions are prevented, visual style and icon both indicate disabled status. Works with all variants |