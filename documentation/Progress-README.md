#Progress
A standard Determinate and Indeterminate Progress element in the Astro Design Language
##Guidelines

* [Astro UXDS: Progress](http://www.astrouxds.com/library/progress)

##Usage
###Determinate
A determinate progress bar makes use of the standard [HTML5 Progress Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress) wrapped in a container div with a class of `rux-progress`. In addition to the graphic progress bar a text representation of the progress is available.

__Note__: Determinate Progress needs a JavaScript hook to increment the value.

```html
<div class='rux-progress'>
	<progress value='77' max='100' role='progressbar'></progress>
	<div class='rux-progress__value'>77%</div>
</div>
```

###Indeterminate
The indeterminate progress bar uses a custom set of divs.

__Note__: Changes to indeterminate progress are imminent.

```html
<div class='rux-progress--indeterminate'>
	<div class='rux-progress--indeterminate__bar'></div>
	<div class='rux-progress--indeterminate__track'></div>
</div>
```

###Attributes
| Attribute       | Description                                                                                                                                                                      
| --------------- | ------- |
| `value`     | The current value of the progress bar. Default behavior is a range from 0-100 |
| `max`     | Override for the default max value of the progress bar |