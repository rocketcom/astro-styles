switchTheme = function(event) {
  // ensure origin is from astrouxds
  if (
    event.origin !== "https://www.astrouxds.com" &&
    event.origin !== "https://astrouxds.com" &&
    event.origin !== "http://cms.astrouxds.com" &&
    event.origin !== "http://cms-dev.astrouxds.com"
  ) {
    return;
  }

  if (event.data === "light-theme") {
    document.body.classList.remove("dark-theme");
    document.body.classList.add("light-theme");
  } else {
    document.body.classList.add("dark-theme");
    document.body.classList.remove("light-theme");
  }
};
window.addEventListener("message", switchTheme, false);

/*
    **
    ** DOCUMENT READY
    ** Jquery replacement in ES6-ish syntax
    **
    */
document.onreadystatechange = () => {
  if (document.readyState === "complete") {
  }
};
